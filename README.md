# Stock panel

An Akka stock panel application for exploring Akka and DDD concepts.

## User Stories
- As a user, I want to create a panel.
- As a user, I want to add a stock to my panel.
- As a user, I want to set my holding for a stock in my panel.
- As a user, I want to see my holding updating in real time.

## Technicals
To show functionality without requiring a real time connection to market pricing, we'll add a configuration with the available stock tickers, their descriptions and initial pricing. The stock movement will be simulated with a random walk.

The main components of the application will be:
- Account Actor, which stores holdings in an account. Initially a user will be equivalent to an account.
- Market, which stores and updates stock pricing.
- Stock pricing will be propagated to the accounts through events subscription (PriceUpdate(ticker)).
- After initial functionality, we'll add storage to database.